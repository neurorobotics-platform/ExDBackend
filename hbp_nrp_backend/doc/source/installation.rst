.. _installation:

Installation
============

:abbr:`NRP(Neurorobotics Platform)` Backend is supposed to be working together with other :term:`NRP` components. For a full source installation, please, refer the :ref:`source installation guide <source-installation>`, or the `instruction from the repository <https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/>`_.
    
.. warning::
    In order to install Backend separately from the other NRP components, you will need to adopt those instructions in according with your needs.
