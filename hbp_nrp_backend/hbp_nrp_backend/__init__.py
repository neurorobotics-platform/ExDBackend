"""
This package contains the python code to run the REST web server and supportive tooling
"""

from hbp_nrp_backend.version import VERSION as __version__  # pylint: disable=unused-import
import os
import time

__author__ = 'GeorgHinkel'


class NRPServicesGeneralException(Exception):
    """
    General exception class that can be used to return meaningful messages
    to the ExD frontend.

    :param message: message displayed to the end user.
    :param error_type: Type of error (like 'CLE Error')
    :param error_code: The HTTP error code to send to the frontend.
    """

    def __init__(self, message, error_type, error_code=500, data=None):
        super(NRPServicesGeneralException, self).__init__()
        # These fields are handled by the front-end JS code.
        self.message = message
        self.error_type = error_type
        self.error_code = error_code
        self.data = data

    def __str__(self):
        return f"{repr(self.message)} ({self.error_type})"


class NRPServicesClientErrorException(NRPServicesGeneralException):
    """
    Exception class for client (4xx) errors. It can be used to return meaningful messages
    to the ExD frontend.

    :param message: message displayed to the end user.
    :param error_code: The HTTP error code to send to the frontend.
    """

    def __init__(self, message, error_type="Client error", error_code=400, data=None):
        super().__init__(message, error_type, error_code, data)


class NRPServicesStateException(NRPServicesGeneralException):
    """
    State exception class that can be used to return meaningful messages to the HBP frontend.

    :param message: message displayed to the end user.
    """

    def __init__(self, message, error_type="Transition error", error_code=400, data=None):
        super().__init__(message, error_type, error_code, data)


class NRPServicesDuplicateNameException(NRPServicesGeneralException):
    """
    Duplicate name exception that can be used to return meaningful messages to the HBP frontend in
    case simulation scripts (transfer-functions or state-machines) or populations contain duplicate
    names.

    :param message: message displayed to the end user.
    """

    def __init__(self, message, error_type="Duplicate name error", error_code=403, data=None):
        super().__init__(message, error_type, error_code, data)


class NRPServicesTransferFunctionException(NRPServicesGeneralException):
    """
    Transfer function exception class that can be used to return meaningful messages
    to the HBP frontend in case source code updates fail.

    :param message: message displayed to the end user.
    """

    def __init__(self, message, error_type="Transfer function error", error_code=400, data=None):
        super().__init__(message, error_type, error_code, data)


class NRPServicesStateMachineException(NRPServicesClientErrorException):
    """
    State machine exception class that can be used to return meaningful messages
    to the HBP frontend in case source code updates fail.

    :param message: message displayed to the end user.
    """

    def __init__(self, message, error_type="State machine error", error_code=400, data=None):
        super().__init__(message, error_type, error_code, data)


class NRPServicesWrongUserException(NRPServicesClientErrorException):
    """
    Exception class that can be used to return meaningful messages
    to the HBP frontend in case an invalid user is detected.

    :param message: message displayed to the end user.
    """

    def __init__(self):
        super(NRPServicesWrongUserException, self).\
            __init__(
                "You need to be the simulation owner to apply your changes "
                "or the simulation should be shared with you for you to be able to access it. \
                If you are supposed to have the access, "
                "try leaving and then re-joining the experiment.",
                error_type="Wrong user",
                error_code=401
            )


class NRPServicesUnavailableROSService(NRPServicesGeneralException):
    """
    Server error with status 500 (internal error) issued when a ROS service is unavailable.
    It can be used to return meaningful messages to the ExD frontend.

    :param message: message displayed to the end user. It contains the text of the
                    corresponding ROS exception
    """

    def __init__(self, message, data=None):
        super().__init__(
            f"ROS service not available: {message}",
            error_type="Unavailable ROS service",
            data=data
        )


def get_date_and_time_string():
    """
    Utility function that returns a string reflecting the current date and time
    with a format that is suitable for file or folder names

    :return: a string containing the date and time under the format
        YYYY-mm-dd_HH-MM-SS
    """
    return '_'.join([time.strftime("%Y-%m-%d"), time.strftime("%H-%M-%S")])
