"""
This package controls the simulations
"""
import sys
import pytz

__author__ = 'GeorgHinkel'

timezone = pytz.timezone('Europe/Zurich')

simulations = []

from hbp_nrp_backend.simulation_control.__Simulation import Simulation

python_version_major = sys.version_info.major
