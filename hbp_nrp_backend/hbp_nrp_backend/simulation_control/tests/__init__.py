"""
This module contains unit test transitions
"""
__author__ = 'GeorgHinkel'

from hbp_nrp_backend.simulation_control import python_version_major

builtins_str = "__builtin__" if python_version_major < 3 else "builtins"
