#the following is required for the unit testing
mock==1.0.1; python_version < "3.3"
testfixtures==3.0.2; python_version=="2.7"
testfixtures>6.0.0; python_version >= "3.0"