"""version string - automatically calculated from the SCM (git)"""
import os
import subprocess


def _get_version():
    try:
        version = subprocess.run(['bash',
                                  f'{os.getenv("HBP")}/user-scripts/nrp_get_scm_version.sh',
                                  'get_scm_version'],
                                 stdout=subprocess.PIPE, check=True).stdout.decode('utf-8')
    except subprocess.CalledProcessError as e:
        raise RuntimeError("The SCM version calculation script failed.\
            Expected path: $HBP/user-scripts/nrp_get_scm_version.sh,\
            check its existence.") from e
    return version


VERSION = _get_version()
