"""
Unit tests for testing the version value
"""

import unittest
import hbp_nrp_commons.version
from unittest.mock import patch

__author__ = "Viktor Vorobev"


class TestVersion(unittest.TestCase):

    def test_version(self):
        self.assertRegex(hbp_nrp_commons.version._get_version(), "^\d+.\d+.\d+")
        self.assertRegex(hbp_nrp_commons.version.VERSION, "^\d+.\d+.\d+")

    @patch('os.getenv', return_value="/non-existing-path")
    def test_version_exception(self, mock_getenv):
        self.assertRaises(RuntimeError, hbp_nrp_commons.version._get_version)
