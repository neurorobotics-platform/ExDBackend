.. _hbp_nrp_commons_dev_space:

ExDBackend Commons developer space
=====================================

This is a helper package. It contains shared functionality used across the NRP Backend. Currently it contains the generated files from pyxb to parse the experiment configuration and bibi files, and state machines description files.

.. toctree::
    :maxdepth: 2

    pyxb_info
    python_api
