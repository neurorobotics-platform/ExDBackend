##
## Target All Components in Package by Default (e.g. make verify)
##

#modules that have tests
TEST_MODULES=hbp_nrp_commons/hbp_nrp_commons/ hbp_nrp_watchdog/hbp_nrp_watchdog/ hbp_nrp_backend/hbp_nrp_backend/ hbp_nrp_cleserver/hbp_nrp_cleserver/

#modules that are installable (ie: ones w/ setup.py)
INSTALL_MODULES=hbp-flask-restful-swagger-master hbp_nrp_commons hbp_nrp_watchdog hbp_nrp_backend hbp_nrp_cleserver

#packages to cover
COVER_PACKAGES=hbp_nrp_commons,hbp_nrp_watchdog,hbp_nrp_backend,hbp_nrp_cleserver

#documentation to build
DOC_MODULES=hbp_nrp_commons/doc hbp_nrp_watchdog/doc hbp_nrp_backend/doc hbp_nrp_cleserver/doc

#pyxbgen schemas to generate
# use repository:file:module notation
# use repository notation the same as used in directory layout (i.e., capital letter)
SCHEMA_GENERATION=Experiments:bibi_configuration.xsd:bibi_api_gen \
                  Experiments:ExDConfFile.xsd:exp_conf_api_gen \
                  Models:robot_model_configuration.xsd:robot_conf_api_gen \
                  Models:environment_model_configuration.xsd:environment_conf_api_gen

SCHEMA_TARGET_LOCATION=hbp_nrp_commons/hbp_nrp_commons/generated/
SCHEMA_REQS?=pyxb==1.2.6

PYTHON_PIP_VERSION?=pip>=19

##
## Individual Component Release Targets
##
verify-hbp_nrp_commons:
	$(MAKE) verify TEST_MODULES=hbp_nrp_commons/hbp_nrp_commons/\
                       INSTALL_MODULES=hbp_nrp_commons\
                       COVER_PACKAGES=hbp_nrp_commons\
                       DOC_MODULES=hbp_nrp_commons/doc/\
                       IGNORE_LINT="$(IGNORE_LINT)|hbp_nrp_cleserver|hbp_nrp_backend|hbp_nrp_watchdog|hbp-flask-restful-swagger-master"

##### DO NOT MODIFY BELOW #####################

ifeq ($(NRP_INSTALL_MODE),user)
        include user_makefile
else
        CI_REPO?=git@bitbucket.org:hbpneurorobotics/admin-scripts.git
        CI_DIR?=$(HBP)/admin-scripts/ContinuousIntegration
        THIS_DIR:=$(PWD)

        FETCH_CI := $(shell \
                if [ ! -d $(CI_DIR) ]; then \
                        cd $(HBP) && git clone $(CI_REPO) > /dev/null && cd $(THIS_DIR);\
                fi;\
                echo $(CI_DIR) )

        include $(FETCH_CI)/python/common_makefile
endif
