"""
This package contains functionality to start the CLE Simulation Factory in a separate process
"""
import sys
from hbp_nrp_cleserver.version import VERSION as __version__  # pylint: disable=W0611

__author__ = "Georg Hinkel"

python_version_major = sys.version_info.major
