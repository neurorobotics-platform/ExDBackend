# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
from RestrictedPython import compile_restricted
from hbp_nrp_cleserver.server.ROSCLEServer import extract_line_number
from unittest import TestCase
import sys

__author__ = "Georg Hinkel, Ugo Albanese"


class TestErrorLineExtraction(TestCase):

    test_code = "# Here are some comments\n" \
                "def test():\n" \
                "    def test_inner():\n" \
                "        foo = None\n" \
                "        # the next line will crash\n" \
                "        bar\n" \
                "    # not the next one\n" \
                "    test_inner()\n"

    expected_error_line_no = 6

    def test_extract_line_number(self):

        error_line_no = self.__get_error_line_number(self.test_code)
        self.assertEqual(self.expected_error_line_no, error_line_no)

    def __get_error_line_number(self, code):
        restricted = compile_restricted(code, 'exec')
        local_env = {}
        exec(restricted, globals(), local_env)
        try:
            local_env['test']()
        except NameError:
            _, _, _traceback = sys.exc_info()
            return extract_line_number(_traceback, 'exec')


if __name__ == '__main__':
    import unittest
    unittest.main()